package ru.studwork;

//Наследуем наш класс от Exception, переопределяем метод toString
//Советую почитать про Exception. Про иерархию исключений в Java,
//а также про виды(checked, unchecked) для подготовки к экзамену
public class NotEvenNumberException extends Exception {

    @Override
    public String toString() {
        return "Current array contains odd numbers!";
    }
}
