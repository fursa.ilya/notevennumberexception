package ru.studwork;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) throws NotEvenNumberException { //прочитать про throws
        int[] arr = new int[10]; //Объявляем массив на 10 чисел
        Scanner sc = new Scanner(System.in); //Scanner для ввода с клавиатуры
        for (int i = 0; i < arr.length; i++) { //проходимся по массиву, заполняем его числами
            System.out.print("arr[" + i + "]: ");
            arr[i] = sc.nextInt();

            if(arr[i] % 2 != 0) { //% деление с остатком. Если остаток не равен 0 значит число нечетное
                throw new NotEvenNumberException(); //Выбрасываем исключение
            }
        }
    }
    
}
